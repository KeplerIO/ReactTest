import React, { Component } from 'react';
import { BrowserRouter, Route } from "react-router-dom";
import './App.css';
import Header from "./components/layout/Header.js"
import TodoPanel from './components/TodoPanel.js';
import NewTodoForm from "./components/NewTodoForm.js"
import AboutPage from "./components/pages/AboutPage.js"
import BrowserRESTClient from "./BrowserRESTClient.js";

class App extends Component {
    state = {
        todoItems: [

        ]
    }

    componentDidMount() {

    }

    markItemComplete = (id) => {
        this.setState({
            todoItems: this.state.todoItems.map(item => {
                if (item.id === id) {
                    item.isCompleted = !item.isCompleted;
                }
                console.log(item);
                return item;
            })
        });
    }

    deleteItem = (id) => {
        this.setState({ todoItems: [...this.state.todoItems.filter(item => item.id !== id)] });
    }

    getNextItemId() {
        let temp = this.state.todoItems.map(item => item.id);
        if (temp.length === 0) {
            return 1;
        } else {
            return (Math.max(...temp) + 1);
        }
    }

    addItem = (title) => {
        let newItem = {
            id: this.getNextItemId(),
            title, //In ES6, if key and value are same
            isCompleted: false
        }
        this.setState({ todoItems: [...this.state.todoItems, newItem] });
    }

    render() {
        let net = new BrowserRESTClient("www.google.com", "GET", null, {"Content-Type": "application/json", "Cache-Control": "no-cache"}, "", true, 0);

        


        return (
            <BrowserRouter>
                <div className="App">
                    <div className="container">
                        <Header />
                        <Route exact path="/" render={props => (
                            <React.Fragment>
                                <NewTodoForm addItem={this.addItem} />
                                <TodoPanel todoItems={this.state.todoItems} markItemComplete={this.markItemComplete} deleteItem={this.deleteItem} />
                            </React.Fragment>
                        )}>
                        </Route>
                        <Route path="/about" component={AboutPage}>

                        </Route>
                        
                    </div>
                </div>
            </BrowserRouter>
        );
    }
}

export default App;
