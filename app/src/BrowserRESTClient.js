class BrowserRESTClient {
    //Private members
    #targetURL;
    #requestHttpMethod;
    #requestPayload;
    #requestHeaders;
    #responseType;
    #isAsync;
    #timeout;
    #responseBody;
    
    //Call the constructor using named parameters if you don't want to supply all parameters!
    constructor(targetURL = "", requestHttpMethod = "GET", requestPayload = "", requestHeaders = {}, responseType = "", isAsync = true, timeout = 0) {
        this.#targetURL = targetURL;
        this.#requestHttpMethod = requestHttpMethod;
        this.#requestPayload = requestPayload;
        this.#requestHeaders = requestHeaders;
        this.#responseType = responseType;
        this.#isAsync = isAsync;
        this.#timeout = timeout;
        console.log("REST client created");
    }

    /*********************************************************/
    /*                    BEGIN ACCESSORS
    /*********************************************************/
    //Getter/Setter - URL
    get targetURL() {
        return this.#targetURL;
    }
    set targetURL(url) {
        this.#targetURL = url;
        return this.#targetURL;
    }
    getTargetURL() {
        return this.#targetURL;
    }
    setTargetURL(url) {
        this.#targetURL = url;
        return this.#targetURL;
    }

    //Getter/Setter - HttpMethod
    get requestHttpMethod() {
        return this.#requestHttpMethod;
    }
    set requestHttpMethod(method) {
        this.#requestHttpMethod = method;
        return this.#requestHttpMethod;
    }
    getRequestHttpMethod() {
        return this.#requestHttpMethod;
    }
    setRequestHttpMethod(method) {
        this.#requestHttpMethod = method;
        return this.#requestHttpMethod;
    }

    //Getter/Setter - Request Payload
    get requestPayload() {
        return this.#requestPayload;
    }
    set requestPayload(payload) {
        this.#requestPayload = payload;
        return this.#requestPayload;
    }
    getRequestPayload() {
        return this.#requestPayload;
    }
    setRequestPayload(payload) {
        this.#requestPayload = payload;
        return this.#requestPayload;
    }

    //Getter/Setter - Request Headers
    get requestHeaders() {
        return this.#requestHeaders;
    }
    set requestHeaders(requestHeaders) {
        this.#requestHeaders = requestHeaders;
        return this.#requestHeaders;
    }
    getRequestHeaders() {
        return this.#requestHeaders;
    }
    setRequestHeaders(requestHeaders) {
        this.#requestHeaders = requestHeaders;
        return this.#requestHeaders;
    }

    //Getter/Setter - Response Type
    get responseType() {
        return this.#responseType;
    }
    set responseType(responseType) {
        this.#responseType = responseType;
        return this.#responseType;
    }
    getResponseType() {
        return this.#responseType;
    }
    setResponseType(responseType) {
        this.#responseType = responseType;
        return this.#responseType;
    }

    //Getter/Setter - isAsync
    get isAsync() {
        return this.#isAsync;
    }
    set isAsync(isAsync) {
        this.#isAsync = isAsync;
        return this.#isAsync;
    }
    getIsAsync() {
        return this.#isAsync;
    }
    setIsAsync(isAsync) {
        this.#isAsync = isAsync;
        return this.#isAsync;
    }

    //Getter/Setter - Timeout
    get timeout() {
        return this.#timeout;
    }
    set timeout(timeout) {
        this.#timeout = timeout;
        return this.#timeout;
    }
    getTimeout() {
        return this.#timeout;
    }
    setTimeout(timeout) {
        this.#timeout = timeout;
        return this.#timeout;
    }
    
    //Getter - Request body
    get responseBody() {
        return this.#responseBody;
    }
    getResponseBody() {
        return this.#responseBody;
    }
    /*********************************************************/
    /*                    END ACCESSORS
    /*********************************************************/
    

    sendRequest() {
        let xhr = new XMLHttpRequest();
        xhr.open(this.#requestHttpMethod, this.#targetURL, this.#isAsync);
        
        Object.keys(this.#requestHeaders).forEach((key)=>{
            let value = this.#requestHeaders[key];
            xhr.setRequestHeader(key, value);
        });

        xhr.responseType = this.#responseType;

        xhr.timeout = this.#timeout;

        //TODO: Implement onload, onerror and onprogress XHR callbacks
        

    }

}

export default BrowserRESTClient;