import React, { Component } from "react";
import PropTypes from "prop-types";

class NewTodoForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            title: ""
        }
    }

    onTitleInput = (event) => {
        this.setState({[event.target.name]: event.target.value});
    }

    onSubmit = (event) => {
        event.preventDefault();
        this.props.addItem(this.state.title);
        this.setState({title: ""});
    }

    render() {
        return(
            <form style={{display: "flex"}} onSubmit={this.onSubmit}>
                <input id="" type="text" name="title" value={this.state.title} placeholder="Add a new item" style={{flex: "10", padding: "5px", height: "25px"}} onChange={this.onTitleInput}/>
                <input id="" className="btnAddItem" type="submit" value="Add" style={{flex: "1"}}/>
            </form>
        )
    }
}

NewTodoForm.propTypes = {
    addItem: PropTypes.func
}

export default NewTodoForm;