import React, { Component } from "react";
import PropTypes from "prop-types";

class TodoItem extends Component {
    generateItemStyle() {
        let isCompleted = this.props.item.isCompleted;

        return {
            padding: "10px",
            borderBottom: "1px #ccc dotted",
            backgroundColor: isCompleted ? "green" : "grey",
            textDecoration: isCompleted ? "line-through" : "none"
        }
    }

    btnDeleteStyle = {
        background: "#ff0000",
        color: "#fff",
        border: "none",
        padding: "5px 10px",
        borderRadius: "50%",
        cursor: "pointer",
        float: "right"
    }

    render() {
        return (
            <div style={this.generateItemStyle()}>
                <p style={{color: "white"}}>
                <input id="chkComplete" type="checkbox" name="" checked={this.props.item.isCompleted} onChange={this.props.markItemComplete.bind(this, this.props.item.id)}/>
                {this.props.item.title}
                <button className="btnDelete" style={this.btnDeleteStyle} onClick={this.props.deleteItem.bind(this, this.props.item.id)}>x</button>
                </p>
            </div>
        );
    }
}

TodoItem.propTypes = {
    item: PropTypes.object.isRequired
}

export default TodoItem;