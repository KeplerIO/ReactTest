import React, { Component } from "react";
import TodoItem from "./TodoItem.js"
import PropTypes from "prop-types";

class TodoPanel extends Component {
    render() {
        return (
            <div className="todoPanel">
                {this.props.todoItems.map((item)=>(<TodoItem key={item.id} item={item} markItemComplete={this.props.markItemComplete} deleteItem={this.props.deleteItem}/>))}
            </div>
        )
    }
}

TodoPanel.propTypes = {
    todoItems: PropTypes.array.isRequired
}

export default TodoPanel;