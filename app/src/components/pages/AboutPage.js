import React from 'react'

function AboutPage() {
    return (
        <React.Fragment>
            <h1>About</h1>
            <p>This is a Todo list app built as a React.js learning experience.</p>
        </React.Fragment>
    )
}

export default AboutPage;
