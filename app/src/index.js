import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

class Index {
    run() {
        ReactDOM.render(<App />, document.getElementById('root'));
    }
}

new Index().run();
